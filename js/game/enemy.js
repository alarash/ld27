//===========================================================================
//
//===========================================================================
GAME.Enemy = GAME.Character.extend({
	//img: "enemyBigTest",//"enemyBase",
	img: "enemyBase",

	speed: 1000,
	damage: 1,

	init: function(obj){
		GAME.Character.init.call(this, obj);

		this.timer = 0;

	},

	onstep: function(delta){
		this.timer += delta;
		if(this.timer >= this.speed){
			this.timer = 0;
			this.onbrain();
		}

	},

	onbrain: function(){
		var player = GAME.Game.instance.player;
		if(player.location == this.location){
			//player.HP -= this.damage;
			player.hurt(this.damage);
		}
	}
});


GAME.EnemyBlob = GAME.Enemy.extend({
	img: "enemyBlob",

	speed: 1000,
	damage: 1,

	HP: 3,
})

GAME.EnemyOrc = GAME.Enemy.extend({
	img: "enemyOrc",
	speed: 1000,
	damage: 2,

	HP: 5,
})

GAME.EnemyDragon = GAME.Enemy.extend({
	img: "enemyDragon",
	speed: 1000,
	damage: 3,

	HP: 15,
})