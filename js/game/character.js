//===========================================================================
// CHARACTERS
//===========================================================================
GAME.Character = lib.Entity.extend({
	HP: 10,
	MP: 0,

	init: function(obj){
		this.location = null;

		this.gold = 0;
		this.loot = [];

		_.extend(this, obj);

		this.maxHP = this.HP;
		this.maxMP = this.MP;

	},

	moveTo: function(loc){
		if(this.location)
			this.location.onexit(this);

		this.location = loc;
		this.location.onenter(this);
	},

	onstep: function(delta){

	},

	hurt: function(damage){
		this.HP -= damage;
		if(this.HP < 0) this.HP = 0;

		GAME.Game.instance.dispatchEvent(new GAME.GameEvent("CHARACTER_HURT", {character: this, amount: damage}))
	},

	heal: function(heal){
		if(this.HP == this.maxHP) return;

		this.HP += heal;
		if(this.HP > this.maxHP) this.HP = this.maxHP;

		GAME.Game.instance.dispatchEvent(new GAME.GameEvent("CHARACTER_HEAL", {character: this, amount: heal}))
	},

	healMP: function(heal){
		if(this.MP == this.maxMP) return;

		this.MP += heal;
		if(this.MP > this.maxMP) this.MP = this.maxMP;

		GAME.Game.instance.dispatchEvent(new GAME.GameEvent("CHARACTER_HEAL_MP", {character: this, amount: heal}))
	},

	onLoot: function(enemy){
		//console.log("onLoot", enemy.loot);
		this.gold += enemy.gold;
		for(var i=0; i < enemy.loot.length; i++){
			if(this.loot.indexOf(enemy.loot[i]) == -1){
				var itemCls = GAME[enemy.loot[i]];
				if(itemCls == null || GAME.playerItemClasses.indexOf(itemCls) != -1) return;

				this.loot.push(enemy.loot[i]);

				GAME.Game.instance.dispatchEvent(new GAME.GameEvent("CHARACTER_LOOT", {character: this, loot: enemy.loot[i]}))

			}
		}
	}
});

//===========================================================================
//
//===========================================================================
GAME.PlayerClass = lib.Class.extend({
	HP: 0,
	MP: 0,

	init: function(obj){
		_.extend(this, obj);
	},

	/*
	apply: function(character){
		character.baseHP = this.HP;
		character.baseMP = this.MP;
	}*/
});

GAME.warriorClass = new GAME.PlayerClass({
	name: "Warrior",
	img: "heroWarrior",

	HP: 10,
	MP: 0,
});

GAME.wizardClass = new GAME.PlayerClass({
	name: "Wizard",
	img: "heroWizard",

	HP: 3,
	MP: 7,
});

GAME.rogueClass = new GAME.PlayerClass({
	name: "Rogue",
	img: "heroRogue",

	HP: 5,
	MP: 5,
});

GAME.playerClasses = [GAME.warriorClass, GAME.wizardClass, GAME.rogueClass]



//===========================================================================
//
//===========================================================================
GAME.Player = GAME.Character.extend({
	//img: "heroRogue",//"heroWizard",//"heroWarrior",//"heroBase",
	instance: null,

	init: function(obj){
		GAME.Player.instance = this;
		this.playerClass = GAME.warriorClass;

		this.slots = [new GAME.BareHand(), null, null];

		GAME.Character.init.call(this, obj);

	},

	reset: function(){
		for(var i = 0; i < this.slots.length; i++){
			if(this.slots[i] != null)
				this.slots[i].reset();
		}
	},

	applyClass: function(){
		this.playerClass.apply(this);

	},

	onstep: function(delta){
		for(var i=0; i < this.slots.length; i++){
			if(this.slots[i] != null)
				this.slots[i].onstep(delta);
		}
	},

	onkeydown: function(key){
		switch(key){
			case "left":
			case "right":
			case "up":
			case "down":
				return this.location.tryMove(this, key);

			case "KEY1":
			case "KEY2":
			case "KEY3":
				return this.location.tryItem(this, this.getKeyItem(key));
		}
	},

	getKeyItem: function(key){
		switch(key){
			case "KEY1": return this.slots[0];
			case "KEY2": return this.slots[1];
			case "KEY3": return this.slots[2];
		}
	},

	applyLoot: function(){
		for(var i=0; i < this.loot.length; i++){
			var itemCls = GAME[this.loot[i]];
			if(itemCls == null) return;
			GAME.playerItemClasses.push(itemCls);

			console.log(itemCls)
			console.log(GAME.playerItemClasses);
		}

		this.loot = [];


	}
});

//===========================================================================
//
//===========================================================================
GAME.Enemy = GAME.Character.extend({
	//img: "enemyBigTest",//"enemyBase",
	img: "enemyBase",

	speed: 1,
	damage: 1,

	init: function(obj){
		GAME.Character.init.call(this, obj);

		this.timer = 0;

	},

	onstep: function(delta){
		this.timer += delta / 1000.0;
		if(this.timer >= this.speed){
			this.timer = 0;
			this.onbrain();
		}

	},

	onbrain: function(){
		var player = GAME.Game.instance.player;
		if(player.location == this.location){
			//player.HP -= this.damage;
			player.hurt(this.damage);
		}
	}
});


GAME.EnemyBlob = GAME.Enemy.extend({
	img: "enemyBlob",

	speed: 1,
	damage: 1,

	HP: 3,
})

GAME.EnemyOrc = GAME.Enemy.extend({
	img: "enemyOrc",
	speed: 1,
	damage: 2,

	HP: 5,
})

GAME.EnemyDragon = GAME.Enemy.extend({
	img: "enemyDragon",
	speed: 1,
	damage: 3,

	HP: 15,
})