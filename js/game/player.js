//===========================================================================
//
//===========================================================================
GAME.PlayerClass = lib.Class.extend({
	HP: 0,
	MP: 0,

	init: function(obj){
		_.extend(this, obj);
	},

});

GAME.warriorClass = new GAME.PlayerClass({
	name: "Warrior",
	img: "heroWarrior",

	HP: 10,
	MP: 0,
});

GAME.wizardClass = new GAME.PlayerClass({
	name: "Wizard",
	img: "heroWizard",

	HP: 3,
	MP: 7,
});

GAME.rogueClass = new GAME.PlayerClass({
	name: "Rogue",
	img: "heroRogue",

	HP: 5,
	MP: 5,
});

GAME.playerClasses = [GAME.warriorClass, GAME.wizardClass, GAME.rogueClass]



//===========================================================================
//
//===========================================================================
GAME.Player = GAME.Character.extend({
	instance: null,

	init: function(obj){
		GAME.Player.instance = this;
		this.playerClass = GAME.warriorClass;

		this.slots = [new GAME.BareHand(), null, null];

		GAME.Character.init.call(this, obj);

	},

	reset: function(){
		for(var i = 0; i < this.slots.length; i++){
			if(this.slots[i] != null)
				this.slots[i].reset();
		}
	},

	applyClass: function(){
		this.playerClass.apply(this);

	},

	onstep: function(delta){
		for(var i=0; i < this.slots.length; i++){
			if(this.slots[i] != null)
				this.slots[i].onstep(delta);
		}
	},

	onkeydown: function(key){
		switch(key){
			case "left":
			case "right":
			case "up":
			case "down":
				return this.location.tryMove(this, key);

			case "KEY1":
			case "KEY2":
			case "KEY3":
				return this.location.tryItem(this, this.getKeyItem(key));
		}
	},

	getKeyItem: function(key){
		switch(key){
			case "KEY1": return this.slots[0];
			case "KEY2": return this.slots[1];
			case "KEY3": return this.slots[2];
		}
	},

	applyLoot: function(){
		for(var i=0; i < this.loot.length; i++){
			var itemCls = GAME[this.loot[i]];
			if(itemCls == null) return;
			GAME.playerItemClasses.push(itemCls);

			console.log(itemCls)
			console.log(GAME.playerItemClasses);
		}

		this.loot = [];


	}
});