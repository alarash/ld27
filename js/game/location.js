GAME.Location = lib.Entity.extend({
	img: "location_null",
	img_enterred: null,

	type: "EMPTY",

	init: function(x, y, obj){
		this.world = null;//world;
		this.x = x;
		this.y = y;

		this.enterred = false;

		this.canMove = true;

		_.extend(this, obj)

	},

	onstep: function(delta){

	},

	tryMove: function(user, dir){
		if(this.canMove){
			if(this.world.dirs[dir] != undefined){
				var n = this.world.getNeighbour(this, dir);
				if(n == null) return;
				if(!n.canenter()) return;
				user.moveTo(n);
			}
		}
	},

	tryItem: function(user, item){
		if(item == null) return;
		return item.use(user, this);
	},

	canenter:function(){
		return true;
	},

	onfirstenter: function(user){

	},

	onenter: function(user){
		if(!this.enterred){
			if(this.img_enterred != null)
				this.img = this.img_enterred;
			this.onfirstenter(user);
		}
		this.enterred = true;

	},

	onexit: function(user){

	}

});

//===========================================================================
//
//===========================================================================
GAME.Village = GAME.Location.extend({
	img: "location_village",
	type: "VILLAGE",

	init: function(x, y, obj){
		GAME.Location.init.call(this, x, y, obj);

		this.timer = 0;
	},

	onstep: function(delta){
		var player = GAME.Game.instance.player;
		if(player.location != this) return;

		this.timer += delta / 1000.0;
		if(this.timer >= 1){
			this.timer = 0;
			//player.HP += 1;
			player.heal(1);
			player.healMP(1);
		}

	}
})

//===========================================================================
//
//===========================================================================
GAME.Battle = GAME.Location.extend({
	img: "location_battle",
	type: "BATTLE",

	init: function(x, y, obj){
		GAME.Location.init.call(this, x, y, obj);
		this.canMove = false;
		this.enemy = null;

		_.extend(this, obj)

		if(this.enemy != null)
			this.enemy.location = this;
	},

	enemyIsDead: function(){
		return this.enemy == null || this.enemy.HP <= 0;
	},

	onstep: function(delta){
		if(this.enemyIsDead()){
			this.canMove = true;
			this.img = "location_battle_win";
		}
		else {
			this.enemy.onstep(delta);
		}
	},
})

//===========================================================================
//
//===========================================================================
GAME.Chest = GAME.Location.extend({
	img: "location_chest",
	type: "CHEST",

	init: function(x, y, obj){
		this.loot = null;
		this.looted = false;
		GAME.Location.init.call(this, x, y, obj);

		if(this.checkLooted()){
			this.looted = true;
			this.img = "location_chest_open";
		}
	},

	onenter: function(user){
		if(this.looted) return;

		this.looted = true;
		this.img = "location_chest_open";

		user.onLoot(this);
	},

	onstep: function(delta){

	},

	checkLooted: function(cls){
		//console.log("checkLooted")

		if(this.loot == null) return true;

		for(var i=0; i < this.loot.length; i++){
			var loot = GAME[this.loot[i]];

			//console.log("in player loot", GAME.Player.instance.loot.indexOf(loot))
			//console.log("in item list", GAME.playerItemClasses.indexOf(loot))

			if(GAME.Player.instance.loot.indexOf(loot) == -1 && GAME.playerItemClasses.indexOf(loot) == -1)
				return false;
		}
		return true;
	}



})

//===========================================================================
//
//===========================================================================
GAME.Lock = GAME.Location.extend({
	locks: {},

	img: "location_lock",
	type: "LOCK",

	index: 0,

	init: function(x, y, obj){
		this.open = false;
		GAME.Location.init.call(this, x, y, obj);
		GAME.Lock.locks[this.index] = this;
	},

	canenter: function(user){
		return this.open;
	},

	setOpen: function(){
		this.open = true;
		this.img = "location_lock_open";
	}
})

GAME.Key = GAME.Location.extend({
	img: "location_key",
	img_enterred: "location_key_open",

	type: "KEY",

	index: 0,
	init: function(x, y, obj){
		//this.enterred = false;
		GAME.Location.init.call(this, x, y, obj);
	},

	onfirstenter: function(user){
		GAME.Lock.locks[this.index].setOpen();
	}

	/*
	onenter: function(user){
		if(this.enterred) return;
		this.enterred = true;
		this.img = "location_key_open";


	}*/



})

//===========================================================================
//
//===========================================================================
GAME.Time = GAME.Location.extend({
	img: "location_time",
	img_enterred: "location_time_open",

	onfirstenter: function(user){
		GAME.Game.instance.resetTimer();
	}

})

GAME.Castle = GAME.Location.extend({
	img: "location_castle",

	onfirstenter: function(user){
		app.selectScene(endScene);
	}
})