//===========================================================================
// WORLD
//===========================================================================
GAME.World = lib.Entity.extend({
	dirs: {
		"left": {x:-1, y:0},
		"right": {x:1, y:0},
		"up": {x:0, y:-1},
		"down": {x:0, y:1}
	},

	init: function(){
		this.locations = [];
		//this.generate();

		this.generator = new GAME.Generator(this);//new GAME.GeneratorRandom(this);
		this.generator.generate();
	},

	onstep: function(delta){
		for(var i=0; i < this.locations.length; i++)
			this.locations[i].onstep(delta);
	},

	/*
	onstep: function(delta){
		this.location.onstep(delta);
	},

	onkeydown: function(key){
		this.location.onkeydown(key);
	},*/

	generate: function(){
		this.locations = [];

		/*
		this.village = this.addLocation(new Village(this, 0, 0));
		var loc = this.addLocation(new Location(this, 0, -1));
		this.village.attachLocation(loc, "up");

		var l = this.addLocation(new Location(this, 0, -2));
		loc.attachLocation(l, "up");
		loc = l;

		var l = this.addLocation(new Location(this, 3, -2));
		loc.attachLocation(l, "left");
		loc = l;
		*/

		this.village = this.addLocation(new GAME.Village(0, 0));
		this.addLocation(new GAME.Location(0, -1))
		this.addLocation(new GAME.Battle(0, -2, {enemy: new GAME.Enemy()}))
		this.addLocation(new GAME.Location(0, -3))
		this.addLocation(new GAME.Location(1, -2))


	},

	addLocation: function(loc){
		this.locations.push(loc);
		loc.world = this;
		return loc;
	},

	getLocation: function(x, y){
		for(var i=0; i < this.locations.length; i++){
			if(this.locations[i].x == x && this.locations[i].y == y)
				return this.locations[i];
		}
		return null;
	},

	getNeighbour: function(loc, dir){
		var x = loc.x + this.dirs[dir].x;
		var y = loc.y + this.dirs[dir].y;

		return this.getLocation(x, y);
	},

	/*
	move: function(loc){
		this.location = loc;
	}*/

});


//===========================================================================
// GENERATOR
//===========================================================================
GAME.Generator = lib.Class.extend({
	init: function(game){
		this.game = game;
	},

	map: [
	"                 C               ",
	"                 4               ",
	"                 ...3...         ",
	"                     . .         ",
	"           d.2.1.....M ..T       ",
	"                 L .             ",
	"           l.2...2 .3.m          ",
	"             .   .               ",
	"           c.. 1...b             ",
	"             . .                 ",
	"             T 2.1.2.a           ",
	"                 .               ",
	"                 V               ",
	],

	locs: {
		"V": ["Village", {}],
		".": ["Location", {}],

		"1": ["Battle", {
			enemy: ["EnemyBlob", {}],
		}],
		"2":["Battle", {
			enemy: ["EnemyBlob", {
				HP: 5,
			}],
		}],
		"3": ["Battle", {
			enemy: ["EnemyOrc", {}]
		}],
		"4": ["Battle", {
			enemy: ["EnemyDragon", {}]
		}],


		"a": ["Chest", {loot: ["WoodSword"]}],
		"b": ["Chest", {loot: ["IronSword"]}],
		"c": ["Chest", {loot: ["RedPotion"]}],
		"d": ["Chest", {loot: ["FireSpell"]}],


		"L": ["Lock", {index: 0}],
		"l": ["Key", {index: 0}],

		"M": ["Lock", {index:1}],
		"m": ["Key", {index:1}],

		"T": ["Time", {}],

		"C": ["Castle", {}]
	},

	generate: function(){
		this.game.locations = [];

		for(var y=0; y < this.map.length; y++){
			for(var x=0; x < this.map[0].length; x++){
				var c = this.map[y][x];
				var l = this.locs[c];

				if(l){
					var loc = this.generateLocation(l[0], x, y, l[1]);
					if(loc == null) continue;
					this.game.addLocation(loc);

					if(c == "V") this.game.village = loc;
				}
			}
		}
	},

	generateLocation: function(clsName, x, y, obj){
		var cls = GAME[clsName];
		if(cls == undefined) return null;

		var new_obj = {}
		for(var k in obj){
			var v = obj[k];
			switch(typeof v){
				case "object":
					if(v.length == 2){
						v = this.generateInstance(v[0], v[1]);
					}
			}
			new_obj[k] = v;
		}

		var loc = new cls(x, y, new_obj);
		return loc;
	},

	generateInstance: function(clsName, obj){
		var cls = GAME[clsName];
		if(cls == undefined) return null;

		var inst = new cls(obj);
		return inst;
	},


})


GAME.GeneratorRandom = GAME.Generator.extend({
	generate: function(){
		this.game.locations = [];

		var village = new GAME.Village(0, 0)
		this.game.village = this.game.addLocation(village);

		var i = 10;
		var loc = village;
		while(i--){
			var l = this.addNeigbour(loc);
			if(l != null)
				loc = l;
		}

	},

	addNeigbour: function(loc){
		var dir = ["left", "right", "up", "down"][Math.floor(Math.random()*4)];
		var x = loc.x + this.game.dirs[dir].x;
		var y = loc.y + this.game.dirs[dir].y;

		if(this.game.getLocation(x, y) == null){
			var loc;
			if(Math.random() > 0.8){
				loc = new GAME.Battle(x, y, {enemy: new GAME.Enemy()})
			}
			else{
				loc = new GAME.Location(x, y)
			}

			this.game.addLocation(loc);
			return loc;
		}
		return null;
	}
})