//===========================================================================
// ITEMS
//===========================================================================
GAME.Item = lib.Entity.extend({
	img: "",
	itemName: "Item",
	desc: "...",

	itemType: "ITEM",

	coolDown: 0,


	init: function(obj){
		lib.Entity.init.call(this, obj);
		_.extend(this, obj);

		this.timer = 0;
	},

	reset: function(){
		this.timer = 0;
	},

	onstep: function(delta){
		if(this.timer > 0){
			this.timer -= delta / 1000.0;
			if(this.timer < 0) this.timer = 0;
		}
	},

	canBeUsed: function(user){
		return this.timer == 0;
	},

	use: function(user, location){
		if(!this.canBeUsed(user)) return false;
		this.timer += this.coolDown;
		return true;

	},
});


//===========================================================================
//
//===========================================================================
GAME.Weapon = GAME.Item.extend({
	damage: 0,
	itemType: "WEAPON",

	use: function(user, location){
		if(!GAME.Item.use.call(this, user, location)) return;
		enemy = location.enemy;
		if(enemy == undefined) return;

		if(enemy.HP <= 0) return;

		//enemy.HP -= this.damage;
		enemy.hurt(this.damage);
		if(enemy.HP == 0){
			user.onLoot(enemy);
		}


	}
});

GAME.BareHand = GAME.Weapon.extend({
	img: "item_hand",
	itemName: "Bare Hand",
	itemType: "WEAPON",

	desc: "doesn't do much damage...",

	coolDown: 1,
	damage: 1,
})

GAME.WoodSword = GAME.Weapon.extend({
	img: "item_woodsword",
	itemName: "Wooden Sword",
	itemType: "WEAPON",

	desc: "better than nothing",

	coolDown: 0.3,
	damage: 1,
})

GAME.IronSword = GAME.Weapon.extend({
	img: "item_ironsword",
	itemName: "Iron Sword",
	itemType: "WEAPON",

	desc: "a basic sword",

	coolDown: 0.3,
	damage: 2,
})

//===========================================================================
//
//===========================================================================
/*
GAME.Potion = GAME.Item.extend({

})*/

GAME.RedPotion = GAME.Item.extend({
	img: "item_redpotion",
	itemName: "Red Potion",
	itemType: "POTION",

	desc: "regain 10 HP",

	coolDown: 999, //one time

	use: function(user, location){
		if(!GAME.Item.use.call(this, user, location)) return;
		user.heal(10);
	}
})

GAME.FireSpell = GAME.Item.extend({
	img: "item_spellfire",
	itemName: "Fire Spell",
	itemType: "SPELL",

	desc: "2 MP: 5 Damages",
	coolDown: 2,
	MP: 2,

	damage: 5,

	canBeUsed: function(user){
		return this.timer == 0 && user.MP >= this.MP;
	},

	use: function(user, location){
		if(!GAME.Item.use.call(this, user, location)) return;
		user.MP -= this.MP;

		enemy = location.enemy;
		if(enemy == undefined) return;

		if(enemy.HP <= 0) return;

		enemy.hurt(this.damage);
		if(enemy.HP == 0){
			user.onLoot(enemy);
		}

	}
})

//===========================================================================
//
//===========================================================================
GAME.playerItemClasses = [null, GAME.BareHand]


