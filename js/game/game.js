//===========================================================================
// GAME
//===========================================================================
var GAME = {};

GAME.Game = lib.Entity.extend("Game", {
	instance: null,

	totalTime: 10,

	init: function(){
		GAME.Game.instance = this;
		lib.Entity.init.call(this);
		this.player = new GAME.Player();

		this.world = null;
	},

	start: function(){
		this.world = new GAME.World();
		this.player.location = this.world.village;

		this.resetTimer();
	},

	resetTimer: function(){
		this.timer = this.totalTime;
		this.starTime = Date.now();
	},

	onstep: function(delta){
		if(this.timer != null && this.timer > 0){
			this.timer = this.totalTime - (Date.now() - this.starTime) / 1000;
		}
		else if(this.timer <= 0){
			this.timer = null;
			this.dispatchEvent(new GAME.GameEvent("TIMER_ZERO"));
		}

		this.world.onstep(delta);
		this.player.onstep(delta);

		if(this.player.HP <= 0){
			this.player.HP = 0;
			this.dispatchEvent(new GAME.GameEvent("DEAD"));
		}

	},

	onkeydown: function(key){
		//this.world.onkeydown(key);
		this.player.onkeydown(key);
	},
});

GAME.GameEvent = lib.Event.extend({
	NAME: "GAME_EVENT",
	init: function(type, obj){
		lib.Event.init.call(this, this.NAME, obj);
		this.subtype = type;
	}
})





//===========================================================================
// GAME OBJECT
//===========================================================================
GAME.GameObject = lib.Entity.extend({

});





