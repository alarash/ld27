var app = new ENGINE.Application({

	width: 160,
	height: 240,

	zoom: 3,

	oncreate: function() {  
		this.game = new GAME.Game();

		this.assets.addImages([
			"titleBG.png",
			"creationBG.png",
			"getreadyBG.png",
			"gameBG.png",
			"endBG.png"
		]);

		this.assets.addImages([
			"background.png",
			"background2.png",
			"button.png",
			"butOK.png",
			"sRedButton.png",
			"sGreenButton.png",
		]);

		this.assets.addImages([
			"location_null.png",
			"location_village.png",
			"location_battle.png",
			"location_battle_win.png",
			"location_chest.png",
			"location_chest_open.png",

			"location_lock.png",
			"location_lock_open.png",
			"location_key.png",
			"location_key_open.png",

			"location_time.png",
			"location_time_open.png",

			"location_castle.png",

		]);

		this.assets.addImages([
			"heroBase.png",
			"heroWarrior.png",
			"heroWizard.png",
			"heroRogue.png",
		]);

		this.assets.addImages([
			"enemyBase.png",
			"enemyBigTest.png",
			"enemyBlob.png",
			"enemyOrc.png",
			"enemyDragon.png",
		]);

		this.assets.addImages([
			"item_hand.png",
			"item_woodsword.png",
			"item_ironsword.png",
			"item_redpotion.png",
			"item_spellfire.png",
		])
	},

	onready: function() {
		//this.layer.resizePixel(2);

		this.selectScene(titleScene);
		//this.selectScene(gameScene);
		//this.selectScene(endScene);
	},

	onkeydown: function(key) {
		key = getKey(key)
		this.dispatch("onkeydown", key);
	},

});

//INPUTS
var getKey = function(key){
	switch(key){
		case "z":
		case "w":
		case "shift":
			return "KEY1";

		case "x":
		case "ctrl":
			return "KEY2";

		case "c":
		case "space":
			return "KEY3";

		default: return key;
	}
}
