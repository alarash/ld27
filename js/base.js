(function () {

var lib = {};
window.lib = lib;

//===========================================================================
// BASE CLASS
//===========================================================================
var Class = function(){}
Class.prototype.init = function() {};
lib.Class = Class;

Class.extend = function(){
	var name = null;
	var def = {}
	if(arguments.length == 1){
		def = arguments[0];
	}
	else if(arguments.length == 2){
		name = arguments[0];
		def = arguments[1];
	}

	var classdef = function() {
		if (arguments[0] !== Class) {
			if(this.init) this.init.apply(this, arguments);
		}
	};

	classdef.prototype = new this(Class);
	classdef.prototype.constructor = classdef;
	classdef.prototype.className = name;
	//classdef.prototype.superClass = this;

	classdef.extend = this.extend;
	classdef.className = name;

	if(name) lib[name] = classdef;

	//def.$ = this.prototype;

	for(k in def){
		classdef.prototype[k] = def[k];
		classdef[k] = def[k];
	}

	return classdef;
}

//===========================================================================
// EVENTS
//===========================================================================
var Event = Class.extend("Event",{
	init: function(type, obj) {
		this.type = type;
		this.obj = obj;
	}
});


var EventDispatcher = Class.extend("EventDispatcher", {
	init: function(){
		this.listeners = {}
	},

	addEventListener: function(type, obj, callback){
		if(this.listeners[type] == undefined)
			this.listeners[type] = [];
		this.listeners[type].push([obj, callback]);
	},

	removeEventListener: function(type, obj, callback){
		this.listeners[type].splice(this.listeners[type].indexOf([obj, callback]), 1);
	},

	dispatchEvent: function(event){
		if(this.listeners[event.type] != undefined){
			for(var i=0; i < this.listeners[event.type].length; i++){
				var obj = this.listeners[event.type][i][0];
				var callback = this.listeners[event.type][i][1];
				
				callback.call(obj, event);
			}
		}
	},
});


//===========================================================================
// ENTITY
//===========================================================================
var EntityEvent = Event.extend("EntityEvent", {
	ADD_CHILD: "AddChild",
	REMOVE_CHILD: "RemoveChild",

	ADD_COMPONENT: "AddComponent",
	REMOVE_COMPONENT: "RemoveComponent",
})

//
var Entity = EventDispatcher.extend("Entity", {
	init: function(){
		EventDispatcher.init.call(this);

		this.children = []
		this.parent = null;

		this.components = {};
	},

	//CHILDREN
	addChild: function(child){
		if(child.parent != null) child.parent.removeChild(child);
		this.children.push(child);
		child.parent = this;

		this.dispatchEvent(new EntityEvent(EntityEvent.ADD_CHILD, child));
		return child;
	},

	removeChild: function(child){
		child.parent = null;
		//this.children.remove(child);
		this.children.splice(this.children.indexOf(child), 1);

		this.dispatchEvent(new EntityEvent(EntityEvent.REMOVE_CHILD, child));
		return child;
	},

	//COMPONENT
	addComponent: function(component){
		var cls = typeof(component);
		var inst = component;
		if(cls == "function"){
			cls = component;
			inst = new component();
			inst.entity = this;
		}

		this.components[inst.className] = inst;
		this[inst.className.toLowerCase()] = inst;

		this.dispatchEvent(new EntityEvent(EntityEvent.ADD_COMPONENT, component));
		return inst;
	},

	getComponent: function(component){
		return this.components[component.className];
	},

	//*BUG* marche pas :/
	removeComponent: function(component){
		var cls = typeof(component);
		if(cls == "function"){
			cls = component;
		}
		delete this.components[cls.className];
		delete this[cls.className.toLowerCase()];

		this.dispatchEvent(new EntityEvent(EntityEvent.REMOVE_COMPONENT, component));
		return cls;
	},

});

//===========================================================================
// COMPONENT
//===========================================================================
var Component = Class.extend("Component", {
	name: "",

	init: function(){
		this.entity = null;
	},
});



//===========================================================================
// 
//===========================================================================
})()