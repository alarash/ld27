var utils = {
	pointInRect: function(x, y, obj){
		if(x < obj.x || x > obj.x + obj.width || y < obj.y || y > obj.y + obj.height) return false;
		return true;
	}
}

var GUI = lib.Class.extend({
	init: function(obj){
		this.x = 0;
		this.y = 0;
		this.width = 0;
		this.height = 0;

		_.extend(this, obj);
	},

	update: function(delta){

	},

	render: function(delta){

	},

	contains: function(x, y){
		//return false;
		return utils.pointInRect(x, y, this);

	},

	onmousedown: function(x, y){

	},

	onmouseup: function(x, y){

	},
});
GUI.currentDownGUI = null;

var Frame = GUI.extend({
	img: "background",

	render: function(delta){
		app.buffer.borderImage(
			app.assets.image(this.img),
			this.x, this.y, this.width, this.height,
			12, 12, 12, 12,
			true);
	}
});

var Button = GUI.extend({
	init: function(obj){
		this.label = "...";
		this.img = null;

		this.callback = function(){
			console.log(this.label+" Clicked");
		}

		GUI.init.call(this, obj);

		this.isClicked = false;
	},

	onmousedown: function(x, y){
		//console.log("onmousedown")
		this.isClicked = true;
		GUI.currentDownGUI = this;
	},

	onmouseup: function(x, y){
		//console.log("onmouseup")
		this.isClicked = false;

		if(this.callback){
			if(typeof this.callback == "object" && this.callback.length == 2){
				//console.log(this.callback)

				this.callback[1].call(this.callback[0]);
			}
			else
				this.callback.call(this);
		}

	},

	render: function(delta){
		if(this.img){
			app.buffer.drawImage(app.assets.image(this.img), this.x, this.y);
		}
		else {
			app.buffer
				.fillStyle(this.isClicked?"#ff0":"#f00")
				.fillRect(this.x, this.y, this.width, this.height)
				.fillStyle("#fff")
				.font("16px arial")
				.fillText(this.label, this.x + 5, this.y + this.height - 5)
		}
	}

});

var FrameButton = Button.extend({
	render: function(delta){
		Frame.render.call(this,delta);

		app.buffer
			.fillStyle("#fff")
			.font("16px arial")
			.fillText(this.label, this.x + 5, this.y + this.height - 5)
	}
})

var Label = GUI.extend({
	filleStyle: "#fff",
	font: "16px arial",

	init: function(obj){
		this.label = "...";
		GUI.init.call(this, obj);
	},

	render: function(delta){
		app.buffer
			.fillStyle(this.fillStyle)
			.font(this.font)
			.fillText(this.label, this.x, this.y)
	}
})