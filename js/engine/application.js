ENGINE.Application = function(args) {

	var app = this;

	_.extend(this, args);

	this.canvas = document.createElement("canvas");
	this.canvas.width = this.width * this.zoom;
	this.canvas.height = this.height * this.zoom;
	document.body.appendChild(this.canvas)

	var ctx = this.canvas.getContext("2d");
	ctx.imageSmoothingEnabled = false;
	ctx.webkitImageSmoothingEnabled = false;
	ctx.mozImageSmoothingEnabled = false;

	this.buffer = cq(this.width, this.height);
	//this.buffer.appendTo("body")
	

	/* bind events to the application - you will understand it later */
	//eveline(this);
	eveline(this, this, this.canvas);

	/* create loader and assets manager */  
	this.loader = new ENGINE.Loader();
	this.assets = new ENGINE.Assets(this.loader); 

	this.oncreate();

	/* fire loader */
	this.loader.ready(function() {
		app.onready();
	});

	this.keys = {};

};

ENGINE.Application.prototype = { 
	dispatch: function(method) {
		if (this.scene && this.scene[arguments[0]]) this.scene[arguments[0]].apply(this.scene, Array.prototype.slice.call(arguments, 1));
	},

	selectScene: function(scene) {
		this.dispatch("onleave");
		this.scene = scene;
		this.dispatch("onenter");
	},

	onstep: function(delta) {
		this.dispatch("onstep", delta);
	},

	onrender: function(delta) {
		//this.buffer.fillStyle("#000").fillRect(0, 0, this.width, this.height)

		this.dispatch("onrender", delta);
		this.canvas.getContext("2d").drawImage(this.buffer.canvas, 0, 0, this.canvas.width, this.canvas.height);
	},

	onkeydown: function(key) {
		this.keys[key] = true;
		this.dispatch("onkeydown", key);
	},

	onkeyup: function(key){
		this.keys[key] = false;
		this.dispatch("onkeyup", key);
	},

	/* mouse events */
	onmousedown: function(x, y) {
		x = Math.floor(x / this.zoom);
		y = Math.floor(y / this.zoom);
		this.dispatch("onmousedown", x, y); 
	},

	onmouseup: function(x, y) { 
		x = Math.floor(x / this.zoom);
		y = Math.floor(y / this.zoom);
		this.dispatch("onmouseup", x, y); 
	},

	onmousemove: function(x, y) { 
		x = Math.floor(x / this.zoom);
		y = Math.floor(y / this.zoom);
		this.dispatch("onmousemove", x, y); 
	},

	onmousewheel: function(delta) { 
		this.dispatch("onmousewheel", x, y); 
	},
};