//===========================================================================
// 
//===========================================================================
var SpriteSheet = function(obj){
	console.log(obj)
	this.obj = obj;
	this.img = app.assets.image("spritesheet");//(this.obj.meta.image);
}
SpriteSheet.prototype = {
	getSprite: function(name){
		return this.obj.frames[name+".png"];
	},

	drawSprite: function(name, x, y){
		x = x || 0;
		y = y || 0;

		var spr = this.getSprite(name);
		if(spr == undefined) return;
		var frame = spr.frame;
		//console.log(this.img, frame);
		app.buffer.drawImage(this.img, frame.x, frame.y, frame.w, frame.h, x, y, frame.w, frame.h);
	},


	renderBorder: function(name, x, y, width, height, top, right, bottom, left, fill){
		var spr = this.getSprite(name);
		if(spr == undefined) return;
		var frame = spr.frame;

		///console.log(frame)

		app.buffer.borderImageSS(
			this.img,
			x, y, width, height,
			frame.x, frame.y, frame.w, frame.h,
			12, 12, 12, 12,
			true);
	}
}

//===========================================================================
//
//===========================================================================
CanvasQuery.Wrapper.prototype.drawSprite = function(spritesheet, name, x, y) {
	spritesheet.drawSprite(name, x, y);
}

CanvasQuery.Wrapper.prototype.borderImageSS = function(image, x, y, w, h, ix, iy, iw, ih, t, r, b, l, fill) {

	// top
	this.drawImage(image, ix + l, iy + 0, iw - l - r, t, x + l, y, w - l - r, t);

	// bottom
	this.drawImage(image, ix + l, iy + ih - b, iw - l - r, b, x + l, y + h - b, w - l - r, b);

	// left
	this.drawImage(image, ix, iy+ t, l, ih - b - t, x, y + t, l, h - b - t);

	// right
	this.drawImage(image, ix + iw - r, iy + t, r, ih - b - t, x + w - r, y + t, r, h - b - t);

	// top-left
	this.drawImage(image, ix, iy, l, t, x, y, l, t);

	// top-right
	this.drawImage(image, ix + iw - r, iy, r, t, x + w - r, y, r, t);

	// bottom-right
	this.drawImage(image, ix + iw - r, iy + ih - b, r, b, x + w - r, y + h - b, r, b);

	// bottom-left
	this.drawImage(image, ix, iy + ih - b, l, b, x, y + h - b, l, b);

	if (fill) {
		if (typeof fill === "string") {
			this.fillStyle(fill).fillRect(x + l, y + t, w - l - r, h - t - b);
		} else {
			this.drawImage(image, ix + l, iy + t, iw - r - l, ih - b - t, x + l, y + t, w - l - r, h - t - b);
		}
	}

}


/*
var SpriteSheet = function(img, size){
	this.img = img
	this.size = size;
}
SpriteSheet.prototype = {

	getRect: function(){
		switch(arguments.length){
			case 1:
				var index = arguments[0];
				return {
				x: (index * this.size) % this.img.width,
				y: this.size * Math.floor((index * this.size) / this.img.width),
				w: this.size,
				h: this.size,
				}

			case 2:
				var x = arguments[0], y = arguments[1];
				return {
					x: x * this.size,
					y: y * this.size,
					w: this.size,
					h: this.size,
				}

			case 4:
				return {
					x: arguments[0] * arguments[2],
					y: arguments[1] * arguments[3],
					w: arguments[2],
					h: arguments[3]
				}
		}
	}
}
*/

//===========================================================================
// 
//===========================================================================
/*
var Sprite = function(spritesheet, obj){
	this.spritesheet = spritesheet
	this.x = 0;
	this.y = 0;
	this.w = 0;
	this.h = 0;

	_.extend(this, obj);
}
Sprite.prototype = {
	render: function(){
		app.buffer.drawImage(this.spritesheet.img, this.x, this.y, this.w, this.h, 0, 0, this.w, this.h);
	}
}

//===========================================================================
// 
//===========================================================================
CanvasQuery.Wrapper.prototype.drawSprite = function(spritesheet, index, x, y) {
	var rect = spritesheet.getRect(index);
	this.context.drawImage(spritesheet.img, rect.x, rect.y, rect.w, rect.h, x, y, rect.w, rect.h);
}
*/