ENGINE.Scene = function(args) {
	this.gui = [];

	_.extend(this, args);


	if (this.oncreate) this.oncreate();
};

ENGINE.Scene.prototype = {

	onenter: function() { },
	onleave: function() { },
	onrender: function() { },
	onstep: function() { },


	//GUI
	renderGUI: function(){
		for(var i=0; i < this.gui.length; i++){
			this.gui[i].render();
		}
	},

	onmousedown: function(x, y){
		//console.log("scene onmousedown",x ,y)
		for(var i=this.gui.length-1; i >= 0; i--){
			if(this.gui[i].contains(x, y)){
				this.gui[i].onmousedown(x, y);
				return
			}
		}
	},

	onmouseup: function(x, y){
		for(var i=this.gui.length-1; i >= 0; i--){
			if(this.gui[i].contains(x, y)){
				this.gui[i].onmouseup(x, y);
				return
			}
		}
		if(GUI.currentDownGUI != null){
			GUI.currentDownGUI.onmouseup(x, y);
			GUI.currentDownGUI = null;
		}
	},

};
