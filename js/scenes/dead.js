var LootItem = GUI.extend({
	item: null,

	init: function(obj){
		Button.init.call(this, obj);
		this.width = 32;
		this.height = 32;

		this.frame = new Frame({
			x: this.x + 33,
			y: this.y,
			width: app.width - this.x - this.width - 11,
			height: 32,
			img: "background2",
		})
	},

	render: function(delta){
		this.frame.render(delta);
		//Button.render.call(this, delta);

		//var item = GAME.Game.instance.player.slots[this.index];
		//console.log(this.index, item)
		if(this.item != null){
			app.buffer.drawImage(app.assets.image(this.item.img), this.x, this.y)

			app.buffer
				.fillStyle("#000")
				.font("10px arial")
				.fillText(this.item.itemName, this.x + 32 + 4, this.y + 10)
				.font("8px arial")
				.fillText(this.item.desc, this.x + 32 + 4, this.y + 10 + 10);

		}

	}
});

var deadScene = new ENGINE.Scene({

	oncreate: function() {  
		this.bgImg = app.assets.image("endBG")

		var label = new Label({
			label: "Loot",
			x: 5,
			y: 80,
			fillStyle: "#FFF"
		});
		this.gui.push(label);

		this.frame = new Frame({
			x: 5,
			y: 5 + 78,
			width: app.width - 10,
			height: app.height - 125,
		});
		this.gui.push(this.frame);

		var w = 32;
		var h = 32;
		this.startButton = new Button({
			label: "OK", 
			x: app.width - w - 5, 
			y: app.height - h - 5, 
			width: w, 
			height: h,

			img: "butOK",

			callback: this.onOK
		});

		this.gui.push(this.startButton); 

	},

	onenter: function(){
/*		this.player = new GAME.Player()
		GAME.Player.instance = this.player;
		this.player.loot = ["Sword", "BareHand"]

		this.items = [];*/
		this.items = [];

		//===========================================================================
		// ITEMS
		//===========================================================================
		for(var i=0; i < GAME.Player.instance.loot.length; i++){
			var itemCls = GAME[GAME.Player.instance.loot[i]]

			var itemButton = new LootItem({
				x: this.frame.x + 5,
				y: this.frame.y + 5 + 36 * i,

				item: itemCls,
			});

			this.items.push(itemButton);
		}
	},

	onleave: function(){

	},


	onstep: function(delta) {

	},

	onrender: function(delta) {

		app.buffer.drawImage(this.bgImg, 0, 0);
		//app.buffer.fillStyle("#000").fillRect(0, 0, app.width, app.height);
		this.renderGUI();

		for(var i=0; i < this.items.length; i++)
			this.items[i].render(delta);

		//this.startButton.render();

	},

	/*
	onkeydown: function(key){
		if(key == "KEY1"){
			this.onOK();
		}
	},*/

	onOK: function(){
		GAME.Player.instance.applyLoot();

		app.selectScene(titleScene);
	},

});
