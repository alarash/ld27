var endScene = new ENGINE.Scene({

	oncreate: function() {
		this.bgImg = app.assets.image("creationBG")

		var label = new Label({
			label: "YOU WIN !!!",
			x: 10,
			y: 50,
			fillStyle: "#FFF",
			font: "25px arial",
		});
		this.gui.push(label);

		this.startButton = new Button({
			label: "OK",
			x: app.width - 32 - 5,
			y: app.height - 32 - 5,
			width: 32,
			height: 32,
			img: "butOK",
			callback: this.onOK
		});
		this.gui.push(this.startButton)

	},

	onenter: function(){},
	onleave: function(){},
	onstep: function(delta) {},

	onrender: function(delta) {
		app.buffer.drawImage(this.bgImg, 0, 0);
		this.renderGUI();
	},

	/*
	onkeydown: function(key){
		if(key == "KEY1"){
			this.onOK();
		}
	},*/

	onOK: function(){
		GAME.Player.instance.applyLoot();
		app.selectScene(titleScene);
	},

});
