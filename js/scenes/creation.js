var ItemButton = Button.extend({
	init: function(obj){
		Button.init.call(this, obj);
		this.width = 32;
		this.height = 32;

		this.frame = new Frame({
			x: this.x + 33,
			y: this.y,
			width: app.width - this.x - this.width - 11,
			height: 32,
			img: "background2",
		})
	},

	render: function(delta){
		this.frame.render(delta);
		Button.render.call(this, delta);

		var item = GAME.Game.instance.player.slots[this.index];
		//console.log(this.index, item)
		if(item != null){
			app.buffer.drawImage(app.assets.image(item.img), this.x, this.y)

			app.buffer
				.fillStyle("#000")
				.font("10px arial")
				.fillText(item.itemName, this.x + 32 + 4, this.y + 10)
				.font("8px arial")
				.fillText(item.desc, this.x + 32 + 4, this.y + 10 + 10);

		}

	}
});



var creationScene = new ENGINE.Scene({

	oncreate: function() { 

		this.bgImg = app.assets.image("creationBG")

		var frame = new Frame({
			x: 5,
			y: 5 + 35,
			width: app.width - 10,
			height: app.height - 78
		});
		this.gui.push(frame);

		var w = 32;
		var h = 32;
		this.startButton = new Button({
			label: "OK", 
			x: app.width - w - 5, 
			y: app.height - h - 5, 
			width: w, 
			height: h,

			img: "butOK",

			callback: this.onStart
		});
		this.gui.push(this.startButton);

		this.classButton = new FrameButton({
			label: "Class",
			x: 10,
			y: frame.y + 10,
			width: frame.width - 10,
			height: 24,

			img: "button",

			callback: [this, this.onChangeClass]
		});
		this.gui.push(this.classButton);

		//===========================================================================
		// HP / MP
		//===========================================================================
		this.hpLabel = new Label({
			label: "HP:",
			x:10,
			y: frame.y + 50	,
			fillStyle: "#000",
		})
		this.gui.push(this.hpLabel)

		this.mpLabel = new Label({
			label: "MP:",
			x:90,
			y: frame.y + 50	,
			fillStyle: "#000",
		})
		this.gui.push(this.mpLabel)

		this.bonus = 0;
		this.bonusHP = new Button({
			x: 10 + 52,
			y: frame.y + 50 - 14,
			img: "sRedButton",
			width: 16,
			height: 16,

			onstep: function(delta){
				if(creationScene.bonus == 0)
					this.img = "sGreenButton";
				else
					this.img = "sRedButton";
			},

			callback: function(){
				creationScene.bonus = 0;
			}
		})
		this.gui.push(this.bonusHP)

		this.bonusMP = new Button({
			x: 10 + 124,
			y: frame.y + 50 - 14,
			img: "sRedButton",
			width: 16,
			height: 16,

			onstep: function(delta){
				if(creationScene.bonus == 1)
					this.img = "sGreenButton";
				else
					this.img = "sRedButton";
			},

			callback: function(){
				creationScene.bonus = 1;
			}
		})
		this.gui.push(this.bonusMP)


		//===========================================================================
		// ITEMS
		//===========================================================================
		for(var i=0; i < 3; i++){
			var itemButton = new ItemButton({
				x: 10,
				y: this.mpLabel.y + 5 + 35 * i,

				img: "button",

				index: i,
				callback: this.onSelectItem
			});

			this.gui.push(itemButton);
		}

	},

	onenter: function(){
		/*GAME.Game.instance.player = new GAME.Player();
		this.player = GAME.Game.instance.player;*/
		this.player = GAME.Game.instance.player;
		this.player.reset();

		this.classButton.label = this.player.playerClass.name
	},

	onleave: function(){

	},


	onstep: function(delta) {
		//ENGINE.Scene.onstep.call(this, delta);
		this.bonusHP.onstep(delta)
		this.bonusMP.onstep(delta)
		this.hpLabel.label = "HP: "+this.calcPlayerHP();
		this.mpLabel.label = "MP: "+this.calcPlayerMP();
	},

	calcPlayerHP: function(){
		return this.player.playerClass.HP + (this.bonus == 0?2:0);
	},

	calcPlayerMP: function(){
		return this.player.playerClass.MP + (this.bonus == 1?2:0);
	},

	onrender: function(delta) {
		app.buffer.drawImage(this.bgImg, 0, 0);


		//this.startButton.render();
		this.renderGUI();

		//app.buffer.fillStyle("#000").fillRect(0,0,app.width, app.height);
		app.buffer.drawImage(app.assets.image(this.player.playerClass.img), (app.width - 64)*0.5, - 15);
	},

	/*
	onkeydown: function(key){
		if(key == "KEY1"){
			this.onStart();
		}
	},*/

	onStart: function(){
		GAME.Player.instance.HP = GAME.Player.instance.maxHP = creationScene.calcPlayerHP();
		GAME.Player.instance.MP = GAME.Player.instance.maxMP = creationScene.calcPlayerMP();

		app.selectScene(getreadyScene);
	},

	onChangeClass: function(){
		console.log(this.player)

		var ind = GAME.playerClasses.indexOf(this.player.playerClass);
		ind = (ind + 1) % GAME.playerClasses.length;

		this.player.playerClass = GAME.playerClasses[ind];
		this.classButton.label = this.player.playerClass.name
	},

	onSelectItem: function(){
		GAME.Player.instance.slots[this.index] = null;

		app.selectScene(inventoryScene);
		inventoryScene.index = this.index;
	}

});
