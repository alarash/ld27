var inventoryScene = new ENGINE.Scene({

	oncreate: function() {
		this.bgImg = app.assets.image("creationBG")

		this.frame = new Frame({
			x: 5,
			y: 5 + 35,
			width: app.width - 10,
			height: app.height - 78
		});
		this.gui.push(this.frame);

		this.items = [];

	},

	onenter: function(){
		for(var i=0; i < this.items.length; i++){
			this.gui.remove(this.items[i]);
		}

		this.items = [];

		//===========================================================================
		// ITEMS
		//===========================================================================
		for(var i=0; i < GAME.playerItemClasses.length; i++){

			var itemButton = new Button({
				x: this.frame.x + 5 + (35 * (i % 4)),
				y: this.frame.y + 5 + (35 * Math.floor(i/4)),
				width:32,
				height:32,

				img: "button",
				index: i,

				render: function(delta){
					Button.render.call(this, delta);
					//console.log(this.index, GAME.playerItemClasses);

					if(this.index == 0) return;
					var itemCls = GAME.playerItemClasses[this.index];
					app.buffer.drawImage(app.assets.image(itemCls.img), this.x, this.y);


					if(itemCls != null){
						if(inventoryScene.checkPlayerHasItem(itemCls) ||
							(itemCls != null && itemCls.itemType == "WEAPON" && inventoryScene.checkPlayerHasWeapon())){
							app.buffer.fillStyle("rgba(0,0,0,0.3)").fillRect(this.x, this.y, 32, 32)
						}
					}
				},

				callback: function(){
					console.log(this.index);
					var player = GAME.Game.instance.player;
					var itemCls = GAME.playerItemClasses[this.index]

					if(itemCls != null){
						if(inventoryScene.checkPlayerHasItem(itemCls) ||
							(itemCls != null && itemCls.itemType == "WEAPON" && inventoryScene.checkPlayerHasWeapon())){
							return;
						}
					}

					var item = null;
					if(itemCls) item = new itemCls();

					player.slots[inventoryScene.index] = item;

					app.selectScene(creationScene);
				}


			});

			this.gui.push(itemButton);
		}
	},

	onleave: function(){

	},


	onstep: function(delta) {

	},

	onrender: function(delta) {
		app.buffer.drawImage(this.bgImg, 0, 0);
		this.renderGUI();
	},

	checkPlayerHasItem: function(itemCls){
		for(var i = 0; i < GAME.Player.instance.slots.length; i++){
			if(GAME.Player.instance.slots[i] != null && GAME.Player.instance.slots[i] instanceof itemCls)
				return true;
		}
		return false;
	},

	checkPlayerHasWeapon: function(){
		for(var i = 0; i < GAME.Player.instance.slots.length; i++){
			if(GAME.Player.instance.slots[i] != null && GAME.Player.instance.slots[i].itemType == "WEAPON")
				return true;
		}
		return false;
	}


});
