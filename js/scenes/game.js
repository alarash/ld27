var TextParticle = lib.Class.extend({
	init: function(obj){
		this.alive = true;
		this.x = 0;
		this.y = 0;

		this.text = ".";
		this.color = new cq.Color()//(1,1,1,1)
		this.font = "20px bold arial";

		_.extend(this, obj);
	},

	onrender: function(delta){
		this.y -= delta / 50.0;
		this.color[3] -= delta / 500.0;
		if(this.color[3] < 0){
			this.color[3] = 0;
			this.alive = false;
		}

		app.buffer
			//.fillStyle("rgba("+this.color[0]+", "+this.color[1]+", "+this.color[2]+", "+this.color[3]+")")
			.fillStyle(this.color.toRgba())
			.font(this.font)
			.fillText(this.text, this.x, this.y)

		//console.log(this.color.toRgba())
	},
});

var ParticleManager = lib.Entity.extend({
	onstep: function(delta){
		var newArray = []
		for(var i=0; i < this.children.length; i++){
			this.children[i].onstep(delta);
			if(this.children[i].alive)
				newArray.push(this.children[i])
		}
		this.children = newArray;
	},

	onrender: function(delta){
		for(var i=0; i < this.children.length; i++){
			this.children[i].onrender(delta);
		}
	}
});


var gameScene = new ENGINE.Scene({

	oncreate: function() {  
		this.bgImg = app.assets.image("gameBG")

		this.particles = new ParticleManager();

		app.game.addEventListener(GAME.GameEvent.NAME, this, this.ongameevent);
	},

	ongameevent: function(event){
		switch(event.subtype){
			case "TIMER_ZERO":
			case "DEAD":
				app.selectScene(deadScene);
				break;

			case "CHARACTER_HURT":
			case "CHARACTER_HEAL":
			case "CHARACTER_HEAL_MP":
			case "CHARACTER_LOOT":
				this.onCharacter(event);
		}
	},

	onenter: function(){
		//this.counter = 10;
		//this.starTime = Date.now();
		this.particles.children = [];

		app.game.start();
	},

	onleave: function(){

	},

	onstep: function(delta) {
		//this.counter = 10 - (Date.now() - this.starTime) / 1000;
		app.game.onstep(delta);

		/*if(app.game.counter <= 0)
			app.selectScene(endScene);*/
	},

	onkeydown: function(key){
		app.game.onkeydown(key);
	},

	onrender: function(delta) {
		if(app.game.timer == null) return;

		app.buffer.drawImage(this.bgImg, 0, 0)

		this.renderWorld();
		this.renderPlayer();
		this.renderLocation();
		this.particles.onrender(delta);
		this.renderCounter();

	},

	onCharacter: function(event){
		var character = event.obj.character;

		var addPaticleOnCharacter = function(character, particle){
			this.particles.addChild(particle);
			particle.y = app.height - 30;
			if(character == app.game.player){
				particle.x = 40;
			}
			else {
				particle.x = app.width - 40;
			}

			particle.x += 10 - 20 * Math.random();
			particle.y += 10 - 20 * Math.random();
		}

		switch(event.subtype){
			case "CHARACTER_HURT":
				var part = new TextParticle({
					text: "-"+event.obj.amount,
					color:new cq.Color([255, 0, 0, 1])
				})
				addPaticleOnCharacter.call(this, character, part)

				break;

			case "CHARACTER_HEAL":
				var part = new TextParticle({
					text: "+"+event.obj.amount,
					color:new cq.Color([0, 255, 0, 1])
				})
				addPaticleOnCharacter.call(this, character, part)
				break;

			case "CHARACTER_HEAL_MP":
				var part = new TextParticle({
					text: "+"+event.obj.amount,
					color:new cq.Color([0, 180, 255, 1])
				})
				addPaticleOnCharacter.call(this, character, part)
				break;


			case "CHARACTER_LOOT":
				var cls = GAME[event.obj.loot];
				var part = new TextParticle({
					text: "+ "+cls.itemName,
					font: "12px arial",
					color:new cq.Color([0, 255, 200, 2])
				})
				addPaticleOnCharacter.call(this, character, part)
				break;
		}
	},



	//=================================================
	renderCounter: function(){
		app.buffer
			.fillStyle("#fff")
			.font("30px arial")
			.fillText(Math.max(0, app.game.timer).toFixed(1), 3, 25);
	},

	renderWorld: function(){
		//render world location node graph
		var currentLoc = app.game.player.location;//app.game.world.location;

		var getLocationImage = function(loc){
			return app.assets.image(loc.img);
		}

		app.buffer.save().translate(app.width * 0.5 - currentLoc.x * 40, app.height * 0.5 - currentLoc.y * 40);

		app.buffer.fillStyle("#ff3").fillRect(currentLoc.x * 40 -18, currentLoc.y * 40 -18, 36, 36)

		for(var i=0; i < app.game.world.locations.length; i++){
			var loc = app.game.world.locations[i];

			app.buffer.drawImage(getLocationImage(loc), loc.x * 40 - 16, loc.y * 40 - 16)
		}

		app.buffer.restore();
	},


	charWidth: 75,
	charHeight: 40,
	charImg: {x:18, y:38},

	renderPlayer: function(){
		//render player

		var player = app.game.player;

		app.buffer.save().translate(3, app.height - this.charHeight - 3);

		app.buffer.fillStyle("rgba(20,20,20,0.5)").fillRect(0, 0, this.charWidth, this.charHeight);

		app.buffer
			.drawImage(app.assets.image(player.playerClass.img), this.charImg.x-32, this.charImg.y-64);

		app.buffer
			.fillStyle("#fff")
			.font("8px arial")
			.fillText("HP:"+player.HP+"/"+player.maxHP, this.charWidth - 35, 28)
			.fillText("MP:"+player.MP+"/"+player.maxMP, this.charWidth - 35, 38)

		//ITEMS
		for(var i=0; i < player.slots.length; i++){
			app.buffer.save().translate(0, (i - player.slots.length) * 35);
			this.renderItem(player.slots[i]);
			app.buffer.restore();
		}

		app.buffer.restore();
	},

	renderItem: function(item){
		app.buffer.fillStyle("rgba(20,20,20,0.5").fillRect(0, 0, 32, 32);
		if(item == null) return;

		app.buffer.drawImage(app.assets.image(item.img), 0, 0);

		//console.log(item)

		//if(item.timer > 0){
		if(!item.canBeUsed(app.game.player)){
			app.buffer.fillStyle("rgba(20,20,20,0.5").fillRect(0, 0, 32, 32);
		}

	},

	renderLocation: function(){
		/*render location
		village: nothing
		shop: options
		battle: enemy stats
		*/

		var location = app.game.player.location;//app.game.world.location;

		switch(location.type){
			case "BATTLE": return this.renderBattle(location);
		}
	},

	renderBattle: function(location){
		if(!location.enemyIsDead()){
			this.renderEnemy(location.enemy);
		}
		else {

		}
	},

	renderEnemy: function(enemy){

		app.buffer.save().translate(app.width - this.charWidth - 3, app.height - this.charHeight - 3);

		app.buffer.fillStyle("rgba(20,20,20,0.5)").fillRect(0, 0, this.charWidth, this.charHeight);

		app.buffer
			.drawImage(app.assets.image(enemy.img), this.charWidth-this.charImg.x-32, this.charImg.y-64);

		app.buffer
			.fillStyle("#fff")
			.font("8px arial")
			.fillText("HP:"+enemy.HP+"/"+enemy.maxHP, 3, 28)
			.fillText("MP:"+enemy.MP+"/"+enemy.maxMP, 3, 38)

		app.buffer.restore()
	}

});
