
var titleScene = new ENGINE.Scene({

	oncreate: function() {  
		this.bgImg = app.assets.image("titleBG")

		var w = 32;
		var h = 32;
		this.startButton = new Button({
			label: "Start", 
			x: app.width - w - 5, 
			y: app.height - h - 5, 
			width: w, 
			height: h,

			img: "butOK",

			callback: this.onStart
		});

		this.gui.push(this.startButton);
	},

	onenter: function(){ },
	onleave: function(){ },
	onstep: function(delta) { },

	onrender: function(delta) {
		app.buffer.drawImage(this.bgImg, 0, 0);

		//this.startButton.render();
		this.renderGUI();


	},

	/*onkeydown: function(key){
		if(key == "KEY1"){
			this.onStart();
		}
	},*/

	onStart: function(){
		//GAME.Game.instance.player = new GAME.Player();
		//this.player = GAME.Game.instance.player;

		app.selectScene(creationScene);
	},

});
