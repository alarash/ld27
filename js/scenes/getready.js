
var getreadyScene = new ENGINE.Scene({
	maxTime: 3,

	oncreate: function() {
		this.bgImg = app.assets.image("getreadyBG")
	},

	onenter: function(){
		this.timer = this.maxTime;
		this.starTime = Date.now();
	},

	onleave: function(){

	},


	onstep: function(delta) {
		this.timer = this.maxTime - (Date.now() - this.starTime) / 1000;

		if(this.timer <= 0)
			app.selectScene(gameScene);
	},

	onrender: function(delta) {
		app.buffer.drawImage(this.bgImg, 0, 0);

		app.buffer
			.fillStyle("#FFF")
			.font("32px arial")
			.fillText("Get Ready in", 5, 50)
			.fillText(Math.ceil(this.timer), app.width * 0.5 - 10, 100)
	}

});